import numpy as np

from Bio import Phylo as phy

from io import StringIO

class grouping:
	def __init__(self, species, diss):
		self.species = species
		self.diss = diss
		self.name = str(species)

class edges:
	def __init__(self, species, distance, neighbor):
		self.species = species
		self.distance = distance
		self.neighbor = neighbor

class nodeWOspecies:
	def __init__(self, s1, s2):
		self.s1 = s1
		self.s2 = s2
		self.name = str(s1)  + str(s2)
		self.UUID = str(s1)  + "," + str(s2)

	def isSame(self, anotherNodeList):
		for anotherNode in anotherNodeList:
			if type(self.s1) == type(anotherNode.s1) and type(self.s2) == type(anotherNode.s2):
				if self.s1 == anotherNode.s1 and self.s2 == anotherNode.s2:
					return True			
				elif self.s1 == anotherNode.s2 and self.s2 == anotherNode.s1:
					return True
			return False
		return False

class node:
	def __init__(self, node, children):
		self.node = node
		self.children = children
		self.UUID = node
	def __str__(self):
		return self.node

class Mtable:
	def __init__(self, groupings, index):
		self.index = index
		self.groupings = groupings
		self.size = len(groupings)
		dictionary = {}
		newTable = []
		newSpecies = []
		for each in groupings:
			dictionary[each.name] = each.diss
			newTable.append(each.diss)
			newSpecies.append(each.species)
		self.dictionary = dictionary
		self.table = np.array(newTable)
		self.species = newSpecies

	#0 indexing
	def get(self, i, j):
		if i >= 0 and j >= 0:
			return self.table[i][j]
		return 0

	def getFromSpecies(self, species):
		return self.dictionary[str(species)]

	def getSpecies(self):
		return self.species

	def getTable(self):
		return self.table

	def getDissfromSpecies(self, s1, s2):
		for each in self.groupings:
			if each.species == s1:
				# return each.diss
				return each.diss[s2 - 1]

	def R(self, i):
		return sum([self.get(i,x) for x in range(0, self.size)])

	def M(self, i, j):
 		return (self.size - 2) * self.get(i, j) -self.R(i)-self.R(j)

	def allM(self):
 		x=[]
 		for i in range(0, self.size - 1):
 			y=[]
 			for j in range(i + 1, self.size):
 				z=self.M(i, j)
 				y.append(z)
 			x.append(y)
 		x = makeSymTable(x)
 		return x

def makeSymTable(lists):
	finalList = [];
	tempList = []
	temp = []
	size = len(lists) + 1
	for i in range(1, size):
		temp.append(i)
	for i in range(0, size):
		for j in range(0, size):
			if i == j:
				tempList.append(0);
			if i < j:
				tempList.append(lists[i][j - temp[i]]);
			if i > j: 
				tempList.append(lists[j][i - temp[j]])
		finalList.append(tempList)
		tempList = []
	finalList = np.array(finalList)
	return finalList

#Finds the indicies of the minimum in the table of M values
def findMin(table):
	returnMin = table[0][0]
	returnX = 0
	returnY = 0
	for x in range(0, len(table)):
		for y in range(0, len(table)):
			if table[x][y] < returnMin:
				returnX, returnY = x, y
				returnMin = table[x][y]
	return returnX, returnY

#Creates new dissimilarity table where x and y were the two closest in the Mtable
#and the table is made of the two species and all others species together in a node
def joinallbut2(x, y, table): #formerly newD()
	bundle = []
	bundleDisX, bundleDisY = 0, 0
	speciesX = table.species[x]
	speciesY = table.species[y]
	for each in table.groupings:
		if each.species != speciesX and each.species != speciesY:
			bundle.append(each.species)
			if type(each.species) is int:
				bundleDisX += table.get(x, each.species - table.index)
				bundleDisY += table.get(y, each.species - table.index)
			else:
				bundleDisX += table.getDissfromSpecies(each.species, x + 1)
				bundleDisY += table.getDissfromSpecies(each.species, y + 1)
	bundleDisX = bundleDisX / len(bundle)
	bundleDisY = bundleDisY / len(bundle)
	firstL = [table.get(x, y), bundleDisX]
	secondL = [bundleDisY]
	newDiss = makeSymTable([firstL, secondL])
	allGroupings = []
	allGroupings.append(grouping(speciesX, newDiss[0]))
	allGroupings.append(grouping(speciesY, newDiss[1]))
	allGroupings.append(grouping(bundle, newDiss[2]))
	return Mtable(allGroupings, table.index)

#Applies the three point formula on a table of two species and one cluster
def threePoint(table):
	distances=[]
	for leaf in range(0, 3):
		d=0
		other=[]
		for x in range(0, 3):
			if x != leaf:
				d += table.get(leaf,x)
				other.append(x)
		d-= table.get(other[0], other[1])
		d/=2
		distances.append(d)
	returnDist = []
	node = nodeWOspecies(table.species[0], table.species[1])
	for x in range(0, 2):
		returnDist.append(edges(table.species[x], distances[x], node))
	return(returnDist)

def finalThreePoint(table):
	distances=[]
	for leaf in range(0, 3):
		d=0
		other=[]
		for x in range(0, 3):
			if x != leaf:
				d += table.get(leaf,x)
				other.append(x)
		d-= table.get(other[0], other[1])
		d/=2
		distances.append(d)
	returnDist = []
	nodes = []
	nodes.append(nodeWOspecies(table.species[0], table.species[2]))
	nodes.append(nodeWOspecies(table.species[1], table.species[2]))
	nodes.append(nodeWOspecies(table.species[2], nodes[0]))
	for x in range(0, 3):
		returnDist.append(edges(table.species[x], distances[x], nodes[x]))
	return(returnDist)

#finds the new row of dissimilarities for newly joined nodes x and y
def findNewDiss(x, y, m):
	newDiss = []
	speciesX = m.species[x]
	speciesY = m.species[y]
	for i in range(0, m.size):
		if m.species[i] != speciesX and m.species[i] != speciesY:
			temp = m.get(y, i) + m.get(x, i) - m.get(x, y)
			newDiss.append(temp/2)
	return grouping([speciesX, speciesY], newDiss)

def findMax(list):
	largest = 0
	for each in list:
		if type(each) == int:
			if each > largest:
				largest = each
	return largest

def newTable(newDiss, m):
	DissList = []
	SpeciesList = []
	DissList.append(newDiss.diss)
	SpeciesList.append(newDiss.species)
	for i in range(0, m.size):
		if m.species[i] != newDiss.species[0] and m.species[i] != newDiss.species[1]:
			SpeciesList.append(m.species[i])
	for i in range(1, len(SpeciesList)):
		tempList = []
		for j in SpeciesList[i + 1 : len(SpeciesList)]:
			if type(j) == list:
				tempList.append(m.getFromSpecies(j))
				break
			else:
				tempList.append(m.getDissfromSpecies(SpeciesList[i], j - m.index + 1))
		DissList.append(tempList)
	DissList = makeSymTable(DissList[0: len(DissList) - 1])
	groupings = []
	for i in range(0, len(SpeciesList)):
		groupings.append(grouping(SpeciesList[i], DissList[i]))
	return Mtable(groupings, m.index + 1)

def startM():
	lists = [[.31,1.01,.75,1.03], [1,.69,.9], [.61,.42],[.37]]
	table = makeSymTable(lists)
	allGroupings = []
	for x in range(0, len(table)):
		allGroupings.append(grouping(x + 1, table[x]))
	return Mtable(allGroupings, 1)

def startNJ(dissimilarities, species):
	table = makeSymTable(dissimilarities)
	allGroupings = []
	for x in range(0, len(table)):
		allGroupings.append(grouping(species[x], table[x]))
	return Mtable(allGroupings, 1)

#Runs neighbor joining when given an input of a nested array ie [[1, 2, 3, 4, 5], [1, 2, 3, 4], [1, 2, 3], [1, 2], [1]] and an array of all species
def nj(dissimilarities, species):
	disMain = startNJ(dissimilarities, species)
	allDist = []
	while disMain.size > 3:
		allm = disMain.allM()
		x, y = findMin(allm)
		joinedXY = joinallbut2(x, y, disMain)
		for each in threePoint(joinedXY):
			allDist.append(each)
		temp = findNewDiss(x, y, disMain)
		disMain = newTable(temp, disMain)
	for each in finalThreePoint(disMain):
		allDist.append(each)
	return allDist

def findCompleteString(string, speciesList):
	temp = ""
	final = ""
	count = 0
	speciesLen = {}
	for each in speciesList:
		speciesLen[str(each)] = 0
	finalList = {}
	speciesSet = set()
	finalSet = set()
	for x in range(len(string) - 1, 0, -1):
		if string[x] == "]":
			count += 1
			temp = "]" + temp
		elif string[x] == "[":
			count -= 1
			temp = "[" + temp
			if count == 0:
				for each in speciesSet:
					if len(temp) > speciesLen[each]:
						speciesLen[each] = len(temp)
						finalList[each] = temp
				speciesSet = set()
				temp = ""
		elif string[x] != "," and string[x] != " ":
			speciesSet.add(string[x])
			temp = string[x] + temp
			if count == 0:
				for each in speciesSet:
					if len(temp) > speciesLen[each]:
						speciesLen[each] = len(temp)
						finalList[each] = temp
				speciesSet = set()
				temp = ""
		elif string[x] == ",":
			temp = "," + temp
		elif string[x] == " ":
			temp = " " + temp
	for each in finalList:
		if not (finalList[each] in finalSet):
			finalSet.add(finalList[each])
			final += finalList[each]
		finalList[each] = finalList[each][:len(finalList[each]) - 1]
	return "[" + final[0 : len(final) - 1] + "]", finalList


def findLastBracket(index, string):
	temp = ""
	index = 0
	openIndex = 0
	closedIndex = 0
	stringIndex = 0
	for x in range(0, len(temp)):
		if temp[x] == "]":
			finalBrack = x
	for x in range(0, index + 1):
		temp += string[x]
		if string[x] == "[":
			openIndex += 1
		if string[x] == "]":
			closedIndex += 1
	while openIndex - closedIndex != 0:
		if string[stringIndex] == "[":
			temp= temp[stringIndex + 1:]
			openIndex -= 1
		else:
			stringIndex += 1
	return temp

def makeTree(distances):
	allLeaves = []
	allNodes = {}
	finalNodes = {}
	finalNodesSet = set()
	finalNodesString = ""
	allNodesSet = set()
	allSpecies = {}
	for each in distances:
		finalNodes[str(each.species)] = each.distance
		finalNodesString += str(each.species) + ","
		if (each.neighbor in allNodesSet):
			allNodes[each.neighbor.name].children.append(each)
			if (type(each.species) == int):
				allSpecies[str(each.species)] = each.distance
		else:
			allNodes[each.neighbor.name] = node(each.neighbor, [each])
			if (type(each.species) == int):
				allSpecies[str(each.species)] = each.distance
			allNodesSet.add(each.neighbor)

	finalNodesString, finalList = findCompleteString(finalNodesString, allSpecies)
	treeStr = ""
	for x in range(0, len(finalNodesString)):
		if finalNodesString[x] == "[":
			treeStr += "(("
		elif finalNodesString[x] == "]":
			treeStr += ")"
			if x != len(finalNodesString) - 1:
				treeStr += ":" + str(finalNodes[finalList[finalNodesString[x - 1]]]) + ")"
		elif finalNodesString[x] == ",":
			treeStr += ","
		elif finalNodesString[x] != " ": 
			treeStr += finalNodesString[x] + ":" + str(finalNodes[finalNodesString[x]])


	sequence = "("
	for each in allNodes:
		index = 0
		for species in allNodes[each].children:
			if type(species.species) == list:
				sequence += "("
				dist = 0
				for spec in species.species:
					dist = species.distance
					if type(spec) == list:
						for eachSpec in spec:
							if type(eachSpec) != list:
								if (str(eachSpec) in allSpecies):
									sequence += str(eachSpec) + ":" + str(allSpecies[str(eachSpec)])
									del allSpecies[str(eachSpec)]
									sequence += ","
					elif (str(spec) in allSpecies):
						sequence += str(spec) + ":" + str(allSpecies[str(spec)])
						del allSpecies[str(spec)]
						sequence += ","
				sequence = sequence[0 : len(sequence) - 1]
				sequence += "):" + str(dist) + ","
	for each in allSpecies:
		sequence += str(each) + ":" + str(allSpecies[each]) + ","
	sequence = sequence[0 : len(sequence) - 1]
	sequence += ")"
	return treeStr[1:]		

def numOfSpecies(nodeWOs):
	x = 1
	if type(nodeWOs) != nodeWOspecies or type(nodeWOs) != node: 
		return 1
	for char in nodeWOs.UUID:
		if char == ",":
			x +=1
	return x
def edgeMatching(edge, listofEdges):
	endpoints = [edge.species, edge.neighbor]
	for mate in listofEdges:
		if mate.species in endpoints and not (mate.neighbor in endpoints):
			return mate
		elif mate.neighbor in endpoints and not (mate.species in endpoints):
			return mate

def getAllSpecies(nodes):
	species = []
	for each in nodes:
		for eachSpec in nodes[each].children:
			species.append(eachSpec.species)
	return species

def drawASCII(tree):
	tree_str = StringIO(tree)
	tree = phy.read(tree_str, format='newick')
	phy.draw_ascii(tree, column_width=30)
	phy.draw(tree)
