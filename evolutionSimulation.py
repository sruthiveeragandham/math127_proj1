import numpy as np
from neighborJoining import nj, makeTree
from Bio import Phylo as phy
from io import StringIO

# Given string of bases ACGT (parent) and rate of mutation (alpha), return one child genome 
def createChild(parent, alpha):
	child=""
	for base in parent:
		p = np.random.uniform(0,1)
		bases=["A","C","G","T"]
		bases.remove(base)
		if (p < alpha/3):
			child += bases[0]
		elif (alpha/3 <= p<2*alpha/3):
			child += bases[1]
		elif (2/3*alpha <= p < alpha ):
			child += bases[2]
		else:
			child += base
	return child

# TreeNodes have a gene sequence, a parent, depth, name, and an array of children
class TreeNode:
	def __init__(self, value, parent=None, name = None):
		self.sequence = value
		self.name = name
		if parent:
			self.parent = parent
			self.depth = parent.depth+1
		else:
			self.parent= self
			self.depth = 0
		self.children = []

	def __str__(self):
		return self.sequence
	def addChild(self, child):
		self.children.append(child)

#Counts the number of leaves for naming purposes
leafNumber = 1

def treeHelper(ancestor, alpha, levels):
	global leafNumber
	if levels ==0:
		ancestor.name = leafNumber
		leafNumber += 1
		return ancestor
	for i in range(2):
		childSequence = createChild(ancestor.sequence, alpha)
		child = TreeNode(childSequence, ancestor)
		ancestor.addChild(child)
		treeHelper(child, alpha, levels-1)
	return ancestor

# Using treeHelper, creates a tree after levels generations from parent sequence seq 
def tree(seq, alpha, levels):
	root = TreeNode(seq)
	treeHelper(root,alpha, levels)
	return root

def printTree(root, level=0):
    print (("   " * level) + str(root))
    for child in root.children:
        printTree(child, level + 1)

def treeCopy(root, parent = None):
	copy = TreeNode(root.sequence, parent, root.name)
	for child in root.children:
		childCopy = treeCopy(child, copy)
		copy.addChild(childCopy)
	return copy

# Fraction of bases that are different between the DNA strings
def hammingDissimilarity(taxon1, taxon2):
	assert(len(taxon1)==len(taxon2))
	diff=0
	for i in range(len(taxon2)):
		if taxon2[i] != taxon1[i]:
			diff+=1
	return diff/len(taxon1)


# Calculates generations t between taxon1 and taxon 2 based on Jukes-Cantor 
def jukescantorDistance(taxon1, taxon2, alpha): 
	a = hammingDissimilarity(taxon1, taxon2)
	return ((-0.75)*(np.log(1-(4/3)*a)))/alpha

# Returns a list of lists, with returnArray[0] being [d(s2-s1), d(s3-s1), d(s4-s1), d(s5-s1)] and so on 
def allDistances_jc(sequenceArray, alpha): 
	returnArray = []
	for i in range(0, len(sequenceArray) - 1):
		returnArray.append([])
		for j in range(i + 1, len(sequenceArray)): 
			returnArray[i].append(jukescantorDistance(sequenceArray[i], sequenceArray[j], alpha))
	return returnArray

def allDistances_h(sequenceArray):
	returnArray = []
	for i in range(0, len(sequenceArray) - 1):
		returnArray.append([])
		for j in range(i + 1, len(sequenceArray)): 
			returnArray[i].append(hammingDissimilarity(sequenceArray[i], sequenceArray[j]))
	return returnArray

# Given a the root of a tree (r), returns a list of all its leaves
def allLeaves(r):
	l = []
	allLeavesHelper(r, l)
	return l 

def allLeavesHelper(root, leaves): 
	if (len(root.children) == 0):
		leaves.append(root)
	else: 
		for child in root.children: 
			allLeavesHelper(child, leaves)

# Given a list of leaf TreeNodes (leaves) and a integer number (num), returns a random collection of the leaf TreeNodes
def randomLeaves(leaves, num): 
	leaf_index_set = set()
	while len(leaf_index_set) < num: 
		leaf_index_set.add(np.random.random_integers(0, len(leaves) -1 ))
	random_leaves = []
	for index in leaf_index_set:
		random_leaves.append(leaves[index])
	return random_leaves

#Find most recent common ancestor of a pair of leaves
def commonAncestor(leaves):
	commonParents = {}
	greatestDepth = 0
	smallestCA = None 
	smallest_child1 = None 
	smallest_child2 = None 
	if len(leaves) == 2: 
		if leaves[0].depth > leaves[1].depth: 
			return leaves[1]
		else: 
			return leaves[0]
	for i in range(0, len(leaves)):
		for j in range (i + 1, len(leaves)): 
			leaf_i = leaves[i]
			leaf_j = leaves[j]
			common = commonParent(leaf_i, leaf_j)
			if (common.depth > greatestDepth): 
				greatestDepth = common.depth
				smallestCA = common
				smallest_child1 = leaf_i
				smallest_child2 = leaf_j 
	return smallest_child1, smallest_child2, smallestCA 

def commonParent(leaf1, leaf2):
	if leaf1.depth < leaf2.depth: 
		while leaf2.depth != leaf1.depth: 
			leaf2 = leaf2.parent
	elif leaf1.depth > leaf2.depth: 
		while leaf1.depth != leaf2.depth: 
			leaf1 = leaf1.parent 
	while leaf1 != leaf2: 
		leaf1 = leaf1.parent
		leaf2 = leaf2.parent 
	return leaf1 

# Given a list of leaf TreeNodes (leaves), returns the correct tree in Newick notation 
def correctTree(leaves):
	while len(leaves)>2:
		s1,s2, parent = commonAncestor(leaves)
		cherry = "("+str(s1)+":"+ str(s1.depth-parent.depth)+ "," + str(s2)+":"+str(s2.depth-parent.depth)+")"
		parent.sequence = cherry
		leaves.append(parent)
		leaves.remove(s1)
		leaves.remove(s2)
	retval = "("+str(leaves[0])+":"+ str(leaves[0].depth)+ "," + str(leaves[1])+":"+str(leaves[1].depth)+")"
	return retval 
	

#gets some of the basic data used to simulate evolution from the user
def getSimulationData():
	parent=input("Enter the starting sequence --> ")
	bases = set(list("ACTG"))
	correct = False
	while not correct:
		correct = True
		for g in parent:
			if not (g in bases):
				correct = False
		if not correct:
			print("Invalid input")
			parent=input("Enter the starting sequence --> ")
	
	generations= int(input("Enter number of generations to simulate --> "))
	correct = False
	while not correct:
		correct = True
		if generations <= 0:
			correct = False
			print("Invalid input")
			generations=int(input("Enter the number of generations --> "))

	numSpe= int(input("Enter the number of species --> "))
	correct = False
	while not correct:
		correct = True
		if numSpe <= 0 or numSpe > 2**(generations):
			correct = False
			print("Invalid input")
			numSpe=int(input("Enter the number of species --> "))
	
	alpha=float(input("Enter alpha --> "))
	correct = False
	while not correct:
		correct = True
		if alpha <= 0 or alpha > .25:
			correct = False
			print("Invalid input")
			alpha=float(input("Enter alpha --> "))
	return parent, generations, numSpe, alpha


#simulates evolution
def simulate():
	parent, generations, numSpe, alpha = getSimulationData()
	root = tree(parent, alpha, generations)
	copy = treeCopy(root)
	rl = randomLeaves(allLeaves(root), numSpe)
	r_leaves_strings = []
	r_leaves_numbers = []
	new_dict = {}
	count = 1 
	print("Your randomly selected members of the population are:")
	for leaf in rl: 
		print(leaf)
		r_leaves_strings.append(leaf.sequence)
		new_dict[count] = leaf.sequence
		r_leaves_numbers.append(count)
		count += 1
	print("You generated the following tree in Newick Notation")
	print(correctTree(rl));
	print("The tree looks like this: (exit the pop-up window to continue)")
	tree_str = StringIO(correctTree(rl))
	treePrint = phy.read(tree_str, format='newick')
	phy.draw_ascii(treePrint, column_width=30)
	phy.draw(treePrint)
	print("The simulated data is ")
	printTree(copy)
	njBool = input("Would you like to try neighbor joining on your simulated data? (Y/N) --> ")
	if (njBool == "Y"):
		ham = input("Use Hamming dissimilarity or Jukes-Cantor distance? \nJ for JC, otherwise default is Hamming --> ")
		if (ham == "J"): 
			dissim = allDistances_jc(r_leaves_strings, alpha)
		else: 
			dissim = allDistances_h(r_leaves_strings)
			print(dissim)
		neighborjoiningHelper(dissim, r_leaves_numbers)
		print("Where species are: ")
		print(new_dict)

def neighborjoiningHelper(dissimilarities, species):
	allDist = nj(dissimilarities, species)
	tree = makeTree(allDist)
	tree_str = StringIO(tree)
	tree = phy.read(tree_str, format='newick')
	phy.draw_ascii(tree, column_width=30)
	phy.draw(tree)
	print("Your tree by neighbor joining is: ")
	print(tree)
	return None

def addnoise():
	parent, generations, numSpe, alpha = getSimulationData()
	root = tree(parent, alpha, generations)
	copy = treeCopy(root)
	rl = randomLeaves(allLeaves(root), numSpe)
	noise= float(input("Enter noise probablity --> "))
	correct = False
	while not correct:
		correct = True
		if noise <= 0 or noise > 1 :
			correct = False
			print("Invalid input")
			noise =float(input("Enter noise probablity --> "))

	for leaf in rl:
		new_seq = ""
		for i in range(len(leaf.sequence)):
			p = np.random.uniform(0,1)
			bases=["A","C","G","T"]
			if (p < noise):
				bases.remove(leaf.sequence[i])
				q = np.random.random_integers(0, 2)
				new_seq = new_seq + bases[q]
			else: 
				new_seq = new_seq + leaf.sequence[i]
		leaf.sequence = new_seq
	new_dict = {}
	count = 1 
	r_leaves_strings = []
	r_leaves_numbers = []
	print("Your randomly selected members of the population are:")
	for leaf in rl: 
		print(leaf)
		r_leaves_strings.append(leaf.sequence)
		new_dict[count] = leaf.sequence
		r_leaves_numbers.append(count)
		count += 1
	print("You generated the following tree in Newick Notation")
	print(correctTree(rl));
	print("The correct tree looks like")
	tree_str = StringIO(correctTree(rl))
	treePrint = phy.read(tree_str, format='newick')
	phy.draw_ascii(treePrint, column_width=30)
	phy.draw(treePrint)
	print("The simulated data is ")
	printTree(copy)
	njBool = input("Would you like to try neighbor joining on your simulated data? (Y/N) --> ")
	if (njBool == "Y"):
		ham = input("Use Hamming dissimilarity or Jukes-Cantor distance? \nEnter J for JC, otherwise default is Hamming --> ")
		if (ham == "J"): 
			dissim = allDistances_jc(r_leaves_strings, alpha)
		else: 
			dissim = allDistances_h(r_leaves_strings)
		neighborjoiningHelper(dissim, r_leaves_numbers)
		print("Where species are: ")
		print(new_dict)


def main():
	print("Enter 'quit' to exit program. ")
	mode=input("Enter 'simulate' to simulate evolution, 'noise' to simulate evolution with noise  --> ")
	while True:
		if mode == "simulate":
			simulate()
		elif mode == "noise":
			addnoise()
		elif mode == "quit":
			return
		else:
			print("That is not a given command")
		mode=input("Enter 'simulate' to simulate evolution, 'noise' to simulate evolution with noise  --> ")


main()